package chtp03;

import java.util.Scanner;


class Rational{
	private int v1;	//有理数的定义

	private int v2;
	public Rational(){
		
	}
	public Rational(int v1,int v2){
		this.v1 = v1;
		this.v2 = v2;
	}	//初始化有理数分子和分母

	
	
	public String Add(Rational a1,Rational a2){	//有理数相加

		int t;
		
		Rational a3 = new Rational();
		a3.v2 = a1.v2*a2.v2;
		a3.v1 = a1.v1*a2.v2+a2.v1*a1.v2;
		t=a3.Gcd(a3.v1, a3.v2);
		a3.v2/=t;
		a3.v1/=t;
		
		return a3.v1+"/"+a3.v2;
	}
	public String Multiply(Rational a1,Rational a2){	//有理数相乘

		Rational a3 = new Rational();
		a3.v2 = a1.v2*a2.v2;
		a3.v1 = a1.v1*a2.v1;
		int t = a3.Gcd(a3.v1, a3.v2);
		a3.v2/=t;
		a3.v1/=t;
		return a3.v1+"/"+a3.v2;
	}
	
	public int Gcd(int m,int n){	//求两个数的最大公约数

		int t;
		while(m%n!=0)
		{
			t=n;
			n=m%n;
			m=t;
		}
		return n;
		
	}
	public String Minus(Rational a1,Rational a2){	//有理数相减

		int t;
		
		Rational a3 = new Rational();
		a3.v2 = a1.v2*a2.v2;
		a3.v1 = a1.v1*a2.v2-a2.v1*a1.v2;
		t=a3.Gcd(a3.v1, a3.v2);
		a3.v2/=t;
		a3.v1/=t;
		
		return a3.v1+"/"+a3.v2;
	}
	
	public String Divide(Rational a1,Rational a2){	//有理数相除

		Rational a3 = new Rational();
		a3.v2 = a1.v2*a2.v1;
		a3.v1 = a1.v1*a2.v2;
		int t = a3.Gcd(a3.v1, a3.v2);
		a3.v2/=t;
		a3.v1/=t;
		return a3.v1+"/"+a3.v2;
	}
	public int getV1() {
		return v1;
	}
	public void setV1(int v1) {
		this.v1 = v1;
	}
	public int getV2() {
		return v2;
	}
	public void setV2(int v2) {
		this.v2 = v2;
	}
}


//201621123037
//秦玉

public class Main3 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Rational r = new Rational();
		Rational r1 = new Rational(sc.nextInt(),sc.nextInt());
		Rational r2 = new Rational(sc.nextInt(),sc.nextInt());
		String a = sc.next();
		while(a.equals("+")||a.equals("-")||a.equals("*")||a.equals("/")) {
			
		if(a.equals("+")){
			System.out.println(r.Add(r1, r2));
			a = sc.next();
		}
		else if(a.equals("-")) {
			System.out.println(r.Minus(r1, r2));
			a = sc.next();
			
		}
		else if(a.equals("*")) {
			System.out.println(r.Multiply(r1, r2));
			a = sc.next();
			
		}
		else if(a.equals("/")) {
			System.out.println(r.Divide(r1, r2));
			a = sc.next();
			
		}
		else {
			System.out.println("error!");
		}
		}
}
}