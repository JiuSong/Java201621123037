package chtp06;


import java.util.Arrays;
import java.util.Scanner;
import java.util.Comparator;

class AgeComparator implements Comparator<PersonSortable>{

	@Override
	public int compare(PersonSortable o1, PersonSortable o2) {
		
		return o1.getAge()-o2.getAge();
	}
	
	
}

class NameComparator implements Comparator<PersonSortable>{

	@Override
	public int compare(PersonSortable o1, PersonSortable o2) {

		return o1.getName().compareTo(o2.getName());
	}

}

class PersonSortable{
	private String name;
	private int age;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return name+"-"+age;
	}
	
	public PersonSortable(String name, int age) {
		this.name = name;
		this.age = age;
	}

	
	
	
}

public class lambda {
	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		sc.nextLine();
		PersonSortable[] persons = new PersonSortable[n];
		for (int i = 0; i < persons.length; i++) {
			persons[i] = new PersonSortable(sc.next(),sc.nextInt());
			sc.nextLine();
		}
		System.out.println("����\n201621123060");
		Arrays.sort(persons,new NameComparator() {
			@Override
			public int compare(PersonSortable o1, PersonSortable o2) {

				return o1.getName().compareTo(o2.getName());
			}
		});
		
		
		
		System.out.println("NameComparator:sort");
		for (int i = 0; i < persons.length; i++) {
			System.out.println(persons[i].toString());
		}
		Arrays.sort(persons,new AgeComparator());
		System.out.println("AgeComparator:sort");
		for (int i = 0; i < persons.length; i++) {
			System.out.println(persons[i].toString());
		}
		System.out.println(Arrays.toString(NameComparator.class.getInterfaces()));
		System.out.println(Arrays.toString(AgeComparator.class.getInterfaces()));
	}
}
