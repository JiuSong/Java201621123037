package chtp06;

public class PersonTest {

	public static void main(String[] args) {
		Person[] peoples = new Person[4];
		
		peoples[0] = new Employee("zhang", "136", "1360", "1360@mail.com", 21, "female", 1000.0);
		peoples[1] = new Student("wang", "110", "15959", "15959@163.com", 18, "male", "1");
		peoples[2] = new Programmer("Gates", "usa", "911", "911@com", 59, "male", 100000.0, 50000.0);
		peoples[3] = new Manager("Clark", "GE", "111", "111@mail.com", 10, "mail", 90000.1, 12000.3);
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++)
				if (peoples[j].getAge() > peoples[j + 1].getAge()) {
					Person temp = peoples[j];
					peoples[j] = peoples[j + 1];
					peoples[j + 1] = temp;
				}
		}
		for (Person person : peoples) {
			System.out.println(person);
		}
		
	}
}

// person类
abstract class Person {
	public String name;
	public String adress;
	public String phonenumber;
	public String email;
	public int age;
	public String gender;

	

	public Person(String name, String adress, String phonenumber, String email, int age, String gender) {
		this.name = name;
		this.adress = adress;
		this.phonenumber = phonenumber;
		this.email = email;
		this.age = age;
		this.gender = gender;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public String getPhonenumber() {
		return phonenumber;
	}

	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}


	@Override
	public String toString() {
		return "Person [name=" + name + ", adress=" + adress + ", phonenumber=" + phonenumber + ", email=" + email
				+ ", age=" + age + ", gender=" + gender + "]";
	}

}

// Student(status常量--freshman, sophomore,junior or senior,)
class Student extends Person {
	private String status;
	
	

	public Student(String name, String adress, String phonenumber, String email, int age, String gender,
			String status) {
		super(name, adress, phonenumber, email, age, gender);
		this.status = status;
	}

	@Override
	public String toString() {
		return "Student [status=" + status + ", toString()=" + super.toString() + "]";
	}

}

// Employee(office,salary,dateHired)
class Employee extends Person {
	public Double salary;

	public Employee(String name, String adress, String phonenumber, String email, int age, String gender,
			Double salary) {
		super(name, adress, phonenumber, email, age, gender);
		this.salary = salary;
	}

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

	@Override
	public String toString() {
		return "Employee [salary=" + salary + ", toString()=" + super.toString() + "]";
	}

	


}

// Employee又有两个子类 Manager(bonus)和Programmer(allowance)
class Manager extends Employee {
	public double bonus;
	
	public Manager(String name, String adress, String phonenumber, String email, int age, String gender, Double salary,
			Double bonus) {
		super(name, adress, phonenumber, email, age, gender, salary);
		this.bonus = bonus;
	}


	@Override
	public String toString() {
		return "Manager [bonus=" + bonus + ", toString()=" + super.toString() + "]";
	}

}

class Programmer extends Employee {
	public double allowance;



	public Programmer(String name, String adress, String phonenumber, String email, int age, String gender,
			Double salary, double allowance) {
		super(name, adress, phonenumber, email, age, gender, salary);
		this.allowance = allowance;
	}



	@Override
	public String toString() {
		return "Programmer [allowance=" + allowance + ", toString()=" + super.toString() + "]";
	}

}
