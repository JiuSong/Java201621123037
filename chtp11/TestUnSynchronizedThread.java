package chtp11;



public class TestUnSynchronizedThread {

	/**
	 * @param args
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		Thread t1 = new Thread(new Adder());
		Thread t2 = new Thread(new Adder());
		Thread t3 = new Thread(new Adder());

		Thread t4 = new Thread(new Subtracter());
		Thread t5 = new Thread(new Subtracter());
		Thread t6 = new Thread(new Subtracter());

		t1.start();
		t2.start();
		t3.start();
		t4.start();
		t5.start();
		t6.start();

		t1.join();
		t2.join();
		t3.join();
		t4.join();
		t5.join();
		t6.join();

		System.out.println(Counter.getId());
		System.out.println("main end");
	}
}

class Adder implements Runnable {

	@Override
	public void run() {
		for (int i = 0; i < 10000; i++)
			Counter.addId();
		System.out.println(Thread.currentThread().getName() + " end");
	}
}

class Subtracter implements Runnable {

	@Override
	public void run() {
		for (int i = 0; i < 10000; i++)
			Counter.subtractId();
		System.out.println(Thread.currentThread().getName() + " end");
	}
}

class Counter {
	private static int id = 0;

	public synchronized static void addId() {
		id++;
	}

	public synchronized static void subtractId() {
		id--;
	}

	public static int getId() {
		return id;
	}
}
