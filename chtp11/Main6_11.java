package chtp11;


import java.util.Scanner;

/*这里放置你的答案，即MyThread类的代码*/

class PrintTask implements Runnable{

	private int num;
	
	public PrintTask(int n) {	
		num = n;
	}
	
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		for (int i = 0; i < num; i++) {
			System.out.println(i);
		}
		System.out.println(Thread.currentThread().getName());
	}
	
}


public class Main6_11 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        PrintTask task = new PrintTask(Integer.parseInt(sc.next()));
        Thread t1 = new Thread(task);
        t1.start();
        sc.close();
    }
}