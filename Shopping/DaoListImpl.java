package Shopping;

import java.util.ArrayList;

public class DaoListImpl implements Dao{
	ArrayList<Goods> shoppingList=new ArrayList<Goods>();
	
	@Override
	public void add(Goods e) {
		shoppingList.add(e);
		
	}
	@Override
	public void delete(Goods e) {
		shoppingList.remove(e);
		
	}
	@Override
	public void clear() {
		if(shoppingList.size()!=0){
	         shoppingList.clear();
	     }
	     System.out.println("  系统已将购物车清空   \n\n");
		
	}
	@Override
	public void display() {
		if(shoppingList.size()==0){
	         System.out.println("    您的购物车是空的  \n\n");
	     }
	     else{
	    	 System.out.println("序号\t商品名\t\t单价\t\t个数\t\t总价");
			 for (int i = 0; i < shoppingList.size(); i++) {
				 if(shoppingList.get(i).GoodsName!=null&&shoppingList.get(i).Price!=0&&shoppingList.get(i).number!=0&&shoppingList.get(i).AllSum!=0) {
			System.out.println((i+1)+"\t"+shoppingList.get(i).getGoodsName()+"\t\t"+shoppingList.get(i).getPrice()+"\t\t"+shoppingList.get(i).getNumber()+"\t\t"+shoppingList.get(i).AllSum);
				 }
			 }
			 
			 System.out.println("");
	     }
		
	}
	

}
