package Shopping;

import java.util.ArrayList;

public interface Dao {
	public void add(Goods e);
	public void delete(Goods e);
	public void clear();
	public void display();

}
