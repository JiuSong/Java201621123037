package chtp10;



import java.util.*;

import javax.annotation.Resource;

class IllegalScoreException extends Exception {

	String msg;

	public IllegalScoreException(String string) {
		this.msg = string;
	}

	@Override
	public String toString() {
		return "IllegalScoreException: " + this.msg;
	}

}

class IllegalNameException extends Exception {

	String msg;

	@Override
	public String toString() {
		return "IllegalNameException: " + this.msg;
	}

	public IllegalNameException(String string) {
		// TODO Auto-generated constructor stub
		this.msg = string;
	}

}

class Student {
	private String name;
	private int score;

	@Override
	public String toString() {
		return "Student [name=" + name + ", score=" + score + "]";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) throws IllegalNameException {
		char[] a = name.toCharArray();
		if (a[0] > 47 && a[0] < 58) {
			throw new IllegalNameException("the first char of name must not be digit, name=" + name);
		}
		this.name = name;

	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public int addScore(int score) throws IllegalScoreException {
		this.score = score;
		if (score < 0 || score > 100)
			throw new IllegalScoreException("score out of range, score=" + score);
		return score;

	}
}

public class Main7_4{

	public static void main(String[] args) {
		Student stu = new Student();
		Scanner sc = new Scanner(System.in);
		String str = sc.next();
		// sc.nextLine();
		while (str.equals("new")) {
		
			try {
				sc.nextLine();
				String name = sc.nextLine();
				String[] str1 = name.split(" ");

				if (str1.length <2) {
					System.out.println("java.util.NoSuchElementException");
					//tr = sc.next();
					continue;
				}
				stu.setName(str1[0]);
				stu.addScore(Integer.parseInt(str1[1]));
				System.out.println(stu.toString());

			} catch (IllegalNameException e) {
				System.out.println(e);

			} catch (IllegalScoreException e) {
				System.out.println(e);

			} catch (Exception e) {
				System.out.println(e);

			} 
			finally {
				str = sc.next();
			}

		}
		System.out.println("scanner closed");
	}

}
