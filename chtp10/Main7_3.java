package chtp10;

import java.util.Scanner;

class ArrayUtils {
	public static double findMax(double[] arr,int begin, int end) throws IllegalArgumentException{
		if(begin>=end)
			throw new IllegalArgumentException("begin:"+begin+" >= end:"+end);
		if(begin<0)
			throw new IllegalArgumentException("begin:"+begin+" < 0");
		if(end>arr.length)
			throw new IllegalArgumentException("end:"+end+" > arr.length");
		double max = arr[begin];
		for (int i = begin+1; i < end; i++) {
			if(arr[i]>max) {
				max = arr[i];
			}
		}
		return max;
		
	}
}


public class Main7_3 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		double[] arr = new double[n];
		int begin,end;
		for (int i = 0; i < n; i++) {
			arr[i] = sc.nextInt();
		}
		while(sc.hasNextInt()) {
			begin = sc.nextInt();
			end = sc.nextInt();
			try {
				double result = ArrayUtils.findMax(arr,begin,end);
				System.out.println(result);
			}catch(IllegalArgumentException e) {
				System.out.println(e);
			}
			
		}
		try {
		     System.out.println(ArrayUtils.class.getDeclaredMethod("findMax", double[].class,int.class,int.class));
		} catch (Exception e1) {
		}
		sc.close();
	}
}
