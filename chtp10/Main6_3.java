package chtp10;

import java.util.EmptyStackException;

class ArrayIntegerStack implements IntegerStack{
    private int capacity;
    private int top=0;
    private Integer[] arrStack;
    
	public Integer push(Integer item)throws FullStackException {
		if(this.size()>=arrStack.length)
			throw new FullStackException();
		if(item == null)
			return null;
		this.arrStack[this.top]=item;
		this.top++;
		return item;
	}
	
	public Integer pop() throws EmptyStackException{ 
		if(this.empty())
			throw new EmptyStackException();
	
		return arrStack[--top];
	}
	

	public Integer peek() throws EmptyStackException {
		if(this.empty()){
			throw new EmptyStackException();
		}
			
		return arrStack[this.top-1];
	}

	
}



public class Main6_3 {

}




