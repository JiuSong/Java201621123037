package chtp10;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class boke5_1 {
	public static void main(String[] args) throws IOException {
		byte[] content = null;
		Scanner sc = new Scanner(System.in);
		FileInputStream fis =null;
		String str = sc.next();
		while(true) {
			try {
				fis = new FileInputStream(str);
			//	fis = new FileInputStream("D\testfis.txt");
				int bytesAvailabe = fis.available();//获得该文件可用的字节数
				if(bytesAvailabe>0){
				 content = new byte[bytesAvailabe];//创建可容纳文件大小的数组
				 fis.read(content);//将文件内容读入数组
				 
				}
				System.out.println(Arrays.toString(content));//打印数组内容
				str = sc.next();
			}catch(FileNotFoundException e) {
				System.out.println("找不到testfis.txt文件，请重新输入文件名");
				str = sc.next();
				
			}
			catch(IOException e) {
				System.out.println("打开或读取文件失败!");
				System.exit(0);
			}
		}
		
}
}