package chtp10;

import java.util.Scanner;

public class Main7_1 {
	public static void main(String[] args) {
		int[] x = new int[5];
		Scanner sc = new Scanner(System.in);
		while(true) {
			try {
				String in = sc.next();
				switch (in) {
				case "arr":
					int num = sc.nextInt();
					int a = x[num];
					break;
				case "null":
					throw new NullPointerException();
				case "cast":
					Object b = new String("HHH");
					Integer b1 = (Integer)b;
					throw new ClassCastException();
				case "num":
					String ch = sc.next();
					Integer c = Integer.parseInt(ch);
					break;
				default:
					break;
				}
				
			}
			catch(ArrayIndexOutOfBoundsException e){
				System.out.println(e);
			}
			catch(NullPointerException e){
				System.out.println(e);
			}
			catch(ClassCastException e){
				System.out.println(e);
			}
			catch(NumberFormatException e){
				System.out.println(e);
			}
		}
		}
	}